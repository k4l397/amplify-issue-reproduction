# Amplify issue reproduction

1. Fork this repo in gitlab
2. `cp infra/sample.tfvars infra/.tfvars`
3. Update `.tfvars` with the fork repos URL
4. Create an access token (repo settings > Access Tokens > Create token with correct set of permissions)
5. Set AWS credentials in environment
6. `cd infra` then `make apply`
7. Start a release in amplify `aws amplify start-job --app-id d1psq4zji7hh0 --branch main --job-type RELEASE`

## Result

The amplify app should attempt a deploy. At the build/frontend stage it should fail with the error:

```

```
